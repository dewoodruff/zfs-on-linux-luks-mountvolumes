#!/bin/bash

#list our zpools to be mounted, one per line, no delimiter
pools=(
	"media"
)
#list all devs and their aliases to be used with luksOpen
declare -A devs=(
	["/dev/sdb"]="sdb-enc"
	["/dev/sdc"]="sdc-enc"
)
#set your log file name
LOG=mountVolumes.log

# the real work happens below
activePools=()
date >> $LOG
function getPoolStatus {
	echo "Checking pool status:" | tee -a $LOG
	for pool in "${pools[@]}"
	do
		echo -en "\t$pool: " | tee -a $LOG
		status=`zpool status $pool 2>&1 | grep "state:" | cut -f2 -d:`
		if [ -z "$status" ];
		then
			echo "unknown - not imported" | tee -a $LOG
		else
			echo $status | tee -a $LOG
			activePools+=($pool)
		fi
	done
}

function exportActivePools {
	if [ -n "$activePools" ];
	then
		echo -n "Exporting pools... " | tee -a $LOG
		for pool in "${activePools[@]}"
		do
			zpool export -f $pool 2>&1 1>>$LOG || { echo "Problem exporting $pool!" | tee -a $LOG; exit 0; }
		done
		echo " done."
	fi
}

function importPools {
	echo -n "Importing pools..."
	for pool in "${pools[@]}"
	do
		zpool import $pool 2>&1 1>>$LOG || { echo "Problem importing $pool!" | tee -a $LOG; exit 0; }
	done
	echo " done."
}

function closeAllLUKS {
	echo "Making sure all LUKS disks are closed..."
	for dev in "${devs[@]}"
	do
		#echo $dev
		cryptsetup close $dev 2>&1 | 1>>$LOG || { echo "Problem closing $dev!" | tee -a $LOG; exit 0; }
		
	done
	echo "Done."
}

function openAllLUKS {
	read -s -p "Enter LUKS passphrase: " pass1
	echo ""
	read -s -p "Confirm LUKS passphrase: " pass2
	echo ""

	if [ "$pass1" = "$pass2" ];
	then
		for dev in "${!devs[@]}"
		do
			echo "Opening $dev to ${devs["$dev"]}" | tee -a $LOG
			echo "$pass1" | cryptsetup luksOpen $dev ${devs[$dev]} 2>&1 1>>$LOG || { echo "Problem opening $dev!" | tee -a $LOG; exit 0; }
		done
	else
		echo "ERROR: passphrases don't match!"
	fi
	pass1=""
	pass2=""

}

function LUKSStatus {
	for dev in "${devs[@]}"
	do
		cryptsetup status $dev | head -1 | tee -a $LOG
	done | sort
}

function unmount {
	zfs unshare -a
	getPoolStatus
	exportActivePools
	closeAllLUKS
	getPoolStatus
}

if [ "$1" = "status" ];
then
	LUKSStatus
	getPoolStatus
elif [ "$1" = "mount" ];
then
	getPoolStatus
	exportActivePools
	closeAllLUKS
	openAllLUKS
	importPools
	getPoolStatus
	zfs share -a
elif [ "$1" = "unmount" ];
then
	unmount
elif [ "$1" = "reboot" ];
then
	unmount
	reboot
elif [ "$1" = "shutdown" ];
then
	unmount
	shutdown -h now
elif [ "$1" = "freespace" ];
then
	zfs list
else
	echo "Usage: ./mountVolumes.sh [status|mount|unmount|reboot|shutdown|freespace]"
fi
